//
//  AddTaskVC.swift
//  TaskStopWatch
//
//  Created by Mati on 10.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit

class AddTaskViewController: UIViewController {
  
  @IBOutlet weak var colorPicker: UIPickerView!
  @IBOutlet weak var colorPickerBtn: UIButton!
  @IBOutlet weak var projectNameTextField: UITextField!
  @IBOutlet weak var taskNameTextField: UITextField!
  var delegate: ViewControllerDelegate?
  
  let colors = ["green", "blue", "yellow", "red", "orange", "pink", "grey"]
  var pickedColor = ""
  var hexColor = 0
  var task: Task?
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let _ = task {
      showData()
    }
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(AddTaskViewController.dismissKeyboard))
    view.addGestureRecognizer(tap)
    
    colorPicker.delegate = self
    colorPicker.dataSource = self
  }
  
  @IBAction func dismissButton(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func colorBtnPressed(_ sender: Any) {
    colorPicker.isHidden = false
  }
  
  func setHexColor() -> Int {
    if pickedColor == "green" {
      hexColor = 0x8DFF44
    } else if pickedColor == "blue" {
      hexColor = 0x0BCFFF
    } else if pickedColor == "yellow" {
      hexColor = 0xEEFF84
    } else if pickedColor == "red" {
      hexColor = 0xFF655C
    } else if pickedColor == "orange" {
      hexColor = 0xFFBD00
    } else if pickedColor == "pink" {
      hexColor = 0xFF98AA
    } else if pickedColor == "grey" {
      hexColor = 0xC8C8C8
    } else {
      hexColor = 0xFFFFFF
    }
  
    return hexColor
  }
  
  func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func showData() {
    projectNameTextField.text = task?.projectName
    taskNameTextField.text = task?.taskName
  }
  
  @IBAction func addTaskBtn(_ sender: Any) {
    
    let date = Date()
    if let task = task {
      let newTask = Task()
      
      newTask.id = task.id
      newTask.projectName = projectNameTextField.text ?? ""
      newTask.taskName = taskNameTextField.text ?? ""
      newTask.date = task.date
      newTask.dateLabel = task.dateLabel
      newTask.color = setHexColor()
      
      DAOManager.shared.update(task: newTask)
    } else {
      let newTask = Task()
      
      newTask.id = DAOManager.shared.incrementID()
      newTask.projectName = projectNameTextField.text ?? ""
      newTask.taskName = taskNameTextField.text ?? ""
      newTask.date = date
      newTask.dateLabel = date.getCurrentDate()
      newTask.color = setHexColor()
    
      DAOManager.shared.add(task: newTask)
    }
    
    delegate?.onClicked()
    dismiss(animated: true, completion: nil)
  }
  
}

extension AddTaskViewController: UIPickerViewDelegate {
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    colorPickerBtn.setTitle(colors[row], for: .normal)
    pickedColor = colors[row]
    colorPicker.isHidden = true
  }
}

extension AddTaskViewController: UIPickerViewDataSource {
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return colors.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return colors[row]
  }
}
