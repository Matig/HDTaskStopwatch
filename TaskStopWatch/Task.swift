//
//  Task.swift
//  TaskStopWatch
//
//  Created by Mati on 13.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object {
  
  dynamic var id = 0
  dynamic var projectName = ""
  dynamic var taskName = ""
  dynamic var dateLabel = ""
  dynamic var date = Date()
  dynamic var color = 0
  
  dynamic var seconds = 0
  dynamic var minutes = 0
  dynamic var hours = 0
  
  
  override class func primaryKey() -> String? {
    return "id"
  }
  
}
