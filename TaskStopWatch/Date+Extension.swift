//
//  Date+Extension.swift
//  TaskStopWatch
//
//  Created by Mati on 03.04.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import Foundation

extension Date {
  func getCurrentDate() -> String {
    let myFormatter = DateFormatter()
    myFormatter.dateFormat = "dd.MM.yyyy"
    return myFormatter.string(from: Date())
  }
  
}
