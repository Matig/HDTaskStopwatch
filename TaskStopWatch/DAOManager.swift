//
//  DAOManager.swift
//  TaskStopWatch
//
//  Created by Mati on 22.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import Foundation
import RealmSwift

class DAOManager {
  
  static let shared = DAOManager()
  private let realm = try! Realm()
  
  private init(){}
  
  func add(task: Task) {
    try! realm.write {
      realm.add(task)
    }
  }
  
  func update(task: Task) {
    try! realm.write {
      realm.add(task, update: true)
    }
  }
  
  func delete(task: Task) {
    try! realm.write({ () -> Void in
      realm.delete(task)
    })
  }
  
  func load() -> [Task] {
    let realm = try! Realm()
    return realm.objects(Task.self).sorted(by: { return $0.date.timeIntervalSince1970 < $1.date.timeIntervalSince1970 })
  }
  
  func incrementID() -> Int {
    let realm = try! Realm()
    return (realm.objects(Task.self).max(ofProperty: "id") as Int? ?? 0) + 1
  }
  
}






