//
//  TableViewController.swift
//  TaskStopWatch
//
//  Created by Mati on 09.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit

protocol ViewControllerDelegate {
  func onClicked()
}

class TableViewController: UITableViewController {
  
  var allTasks: [Task] = []
  let addTaskIdentifier = "addTask"
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    reloadTableView()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView(frame: .zero)
  }
  
  func reloadTableView() {
    allTasks = DAOManager.shared.load()
    tableView.reloadData()
  }
  
  func saveTable() {
    for row in 0 ..< tableView.numberOfRows(inSection: 0) {
      let indexPath = IndexPath(row: row, section: 0)
      let cell = tableView.cellForRow(at: indexPath as IndexPath) as! TableViewCell
      let currentTask = allTasks[indexPath.row]
        
      let newTask = Task()
        
      newTask.id = currentTask.id
      newTask.projectName = currentTask.projectName
      newTask.taskName = currentTask.taskName
      newTask.date = currentTask.date
      newTask.dateLabel = currentTask.dateLabel
      newTask.color = currentTask.color
      newTask.seconds = cell.seconds
      newTask.minutes = cell.minutes
      newTask.hours = cell.hours
      
        
      DAOManager.shared.update(task: newTask)
    }
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == addTaskIdentifier {
      saveTable()
      let viewController = segue.destination as! AddTaskViewController
      
      if let index = sender as? IndexPath {
        viewController.task = allTasks[index.row]
      }
      viewController.delegate = self
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
        
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return allTasks.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.cellIdentifier, for: indexPath) as! TableViewCell
    let currentTask = allTasks[indexPath.row]
  
    cell.loadCellData(task: currentTask)
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (editAction, indexPath) -> Void in
      self.saveTable()
      self.performSegue(withIdentifier: self.addTaskIdentifier, sender: indexPath)
    }
    let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) -> Void in
        self.saveTable()
        DAOManager.shared.delete(task: self.allTasks[indexPath.row])
        self.reloadTableView()
    }
    deleteAction.backgroundColor = .red
    
    return [deleteAction, editAction]
  }
  
}

extension TableViewController: ViewControllerDelegate {
  internal func onClicked() {
    reloadTableView()
  }
}


