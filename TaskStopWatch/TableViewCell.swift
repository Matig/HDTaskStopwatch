//
//  TableViewCell.swift
//  TaskStopWatch
//
//  Created by Mati on 09.03.2017.
//  Copyright © 2017 Mati. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
  
  var task: Task?
  static let cellIdentifier = "taskCell"

  var timer = Timer()
  var hours = 0
  var minutes = 0
  var seconds = 0 {
    didSet{
      showTimeLabel()
    }
  }
  
  var startStopWatch = true
  
  @IBOutlet weak var projectNameLabel: UILabel!
  @IBOutlet weak var taskLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  
  @IBOutlet weak var startStopButton: UIButton!
  @IBOutlet weak var stopWatchTimerLabel: UILabel!
  @IBOutlet weak var resetButton: UIButton!
  
  
  @IBAction func startStop(_ sender: Any) {
    if startStopWatch == true {
      timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateStopWatch), userInfo: nil, repeats: true)
      startStopWatch = false
      startStopButton.setImage(UIImage(named: "pause.png"), for: .normal)
    } else {
      timer.invalidate()
      startStopWatch = true
      startStopButton.setImage(UIImage(named: "play.png"), for: .normal)
    }
  }
  
  func loadCellData(task: Task){
    projectNameLabel.text = task.projectName
    taskLabel.text = task.taskName
    dateLabel.text = task.dateLabel
    seconds = task.seconds
    minutes = task.minutes
    hours = task.hours
    backgroundColor = UIColor(netHex: task.color)
  }
 
  @IBAction func reset(_ sender: Any) {
    seconds = 0
    minutes = 0
    hours = 0
    
    stopWatchTimerLabel.text = "000:00:00"
  }
  
  func updateStopWatch() {
    
    seconds += 1
    if seconds == 60 {
      minutes += 1
      seconds = 0
    }
    
    if minutes == 60 {
      hours += 1
      minutes = 0
    }
    showTimeLabel()
  }
  
  func showTimeLabel() {
    let secondsString = seconds > 9 ? "\(seconds)" : "0\(seconds)"
    let minutesString = minutes > 9 ? "\(minutes)" : "0\(minutes)"
    let hourString = hours > 9 ? "\(hours)" : "00\(hours)"
    
    stopWatchTimerLabel.text = "\(hourString):\(minutesString):\(secondsString)"
  }
}



